#ifndef _levelzdb_h
#define _levelzdb_h

#include "levelz.h"
#include <leveldb/c.h>

#define LEVELZDB_SUCCESS 0
#define LEVELZDB_ERROR 1

#ifdef __cplusplus
extern "C" {
#endif
   
typedef struct _levelzdb_t levelzdb_t;
levelzdb_t *
levelzb_new (char *dbpath);

void
levelzdb_err_reset (levelzdb_t *self);

void
levelzdb_put_string (levelzdb_t *self, char *key, size_t key_len,
        char *val, size_t val_len); 

void
levelzdb_put_zframes (levelzdb_t *self, zframe_t *key, zframe_t *val);

zmsg_t *
levelzdb_get_zframes (levelzdb_t *self, zframe_t *key);

char *
levelzdb_get_string (levelzdb_t *self, char *key, size_t key_len);

void 
levelzdb_del_key (levelzdb_t *self, char *key, size_t key_len);

void 
levelzdb_del_frame (levelzdb_t *self, zframe_t *key);

void
levelzdb_close_db (levelzdb_t *self);

void
levelzdb_destroy_db (levelzdb_t *self);

void
levelzdb_free (levelzdb_t **self_p);

int
levelzdb_test (void);

#ifdef __cplusplus
}
#endif

#endif
