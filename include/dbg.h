/* Originally from Zed Shaw's Awesome Debug Macros 
 * Ansi color support, log_passed, log_failed 
 * and  test macro added by Brian Knox */

#ifndef __dbg_h__
#define __dbg_h__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "ansi_colors.h"

#ifdef NDEBUG
#define debug(M, ...)
#else
#define debug(M, ...) fprintf(stderr, "DEBUG %s:%d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#endif

#define clean_errno() (errno == 0 ? "None" : strerror(errno))

#define log_err(M, ...) fprintf(stderr, "[ERROR] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_warn(M, ...) fprintf(stderr, "[WARN] (%s:%d: errno: %s) " M "\n", __FILE__, __LINE__, clean_errno(), ##__VA_ARGS__)

#define log_info(M, ...) fprintf(stderr, "[INFO] (%s:%d) " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define log_passed(M, ...) fprintf(stderr, ANSI_COLOR_GREEN "[PASSED] " ANSI_COLOR_RESET "(%s:%d) " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define log_failed(M, ...) fprintf(stderr, ANSI_COLOR_RED "[FAILED] " ANSI_COLOR_RESET "(%s:%d) " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)

#define check(A, M, ...) if(!(A)) { log_err(M, ##__VA_ARGS__); errno=0; goto error; }

#define sentinel(M, ...)  { log_err(M, ##__VA_ARGS__); errno=0; goto error; }

#define check_mem(A) check((A), "Out of memory.")

#define check_debug(A, M, ...) if(!(A)) { debug(M, ##__VA_ARGS__); errno=0; goto error; }

#define test(A, M, ...) if(!(A)) { log_failed(M, ##__VA_ARGS__); errno=0; goto failed; } else { log_passed(M, ##__VA_ARGS__);}

#ifdef __cplusplus
}
#endif

#endif
