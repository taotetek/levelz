CC=clang

check: levelz_selftest

levelz_selftest:
	$(CC) -o levelz_selftest levelz_selftest.c levelzdb.c -I. -I./include -I/usr/local/include -lczmq -lleveldb -g -Wall
	./levelz_selftest

clean:
	rm -f levelz_selftest
