#include "levelz.h"

struct _levelzdb_t {
    leveldb_t *db;
    leveldb_options_t *options;
    leveldb_readoptions_t *roptions;
    leveldb_writeoptions_t *woptions;
    char *dbpath;
    char *error;
    int result;
    size_t val_len;
};

levelzdb_t * 
levelzdb_new (char *dbpath) 
{
    assert (dbpath);
    
    levelzdb_t *self;
    self = (levelzdb_t *) zmalloc (sizeof (levelzdb_t));

    self->dbpath = dbpath? strdup(dbpath): NULL;
    self->options = leveldb_options_create ();
    leveldb_options_set_create_if_missing (self->options, 1);
    self->db = leveldb_open (self->options, dbpath, &self->error);
    self->woptions = leveldb_writeoptions_create ();
    self->roptions = leveldb_readoptions_create ();
    self->error = NULL;
    return self;
}

void
levelzdb_error_reset (levelzdb_t *self)
{
    leveldb_free (self->error);
    self->error = NULL;
    self->result = LEVELZDB_SUCCESS;
}
    
void
levelzdb_put_string (levelzdb_t *self, char *key, size_t key_len,
        char *val, size_t val_len)
{
    leveldb_put (self->db, self->woptions, key, 
            key_len, val, val_len, &self->error);

    if (self->error != NULL) 
        self->result = LEVELZDB_ERROR;
    else
        levelzdb_error_reset (self);
}

void 
levelzdb_put_zframe (levelzdb_t *self, zframe_t *key_frame, zframe_t *val_frame)
{
    char *key = zframe_strdup (key_frame);
    size_t key_len = zframe_size (key_frame);

    char *val = zframe_strdup (val_frame);
    size_t val_len = zframe_size (val_frame);

    levelzdb_put_string (self, key, key_len, val, val_len);
    free (val);
}

char *
levelzdb_get_string (levelzdb_t *self, char *key, size_t key_len)
{
    char *val = leveldb_get (self->db, self->roptions, 
            key, key_len, &self->val_len, &self->error);

    if (self->error != NULL)
        self->result = LEVELZDB_ERROR;
    else 
        levelzdb_error_reset (self);

    return val;
}

zframe_t *
levelzdb_get_zframe (levelzdb_t *self, zframe_t *key_frame)
{
    char *key= zframe_strdup (key_frame);
    size_t key_len = zframe_size (key_frame);
    char *val = levelzdb_get_string (self, key, key_len);
    zframe_t *val_frame = zframe_new (val, self->val_len);
    free (key);
    free (val);
    return val_frame;
}

void
levelzdb_del_key (levelzdb_t *self, char *key, size_t key_len)
{
    leveldb_delete (self->db, self->woptions, key, key_len, &self->error);

    if (self->error != NULL)
        self->result = LEVELZDB_ERROR;
    else
        levelzdb_error_reset (self);

    levelzdb_error_reset (self);
}

void
levelzdb_del_frame (levelzdb_t *self, zframe_t *key)
{
    levelzdb_del_key (self, zframe_strdup (key), zframe_size (key));
}

void
levelzdb_close_db (levelzdb_t *self)
{
    leveldb_close (self->db);
}

void
levelzdb_destroy_db (levelzdb_t *self)
{
    leveldb_destroy_db (self->options, self->dbpath, &self->error);

    if (self->error != NULL)
        self->result = LEVELZDB_ERROR;
    else
        levelzdb_error_reset (self);
}

void 
levelzdb_free (levelzdb_t **self_p) 
{
    assert (self_p);
    if (*self_p) {
        levelzdb_t *self = *self_p;
        free (self->dbpath);
        free (self);
        *self_p = NULL;
    }
}

int 
levelzdb_test (void)
{
    levelzdb_t *lz = levelzdb_new ("./testdb");
    test (lz, "levelzdb_new created leveldb database");

    levelzdb_put_string (lz, "key", 3, "value", 6);
    test ((lz->result == 0), "levelzdb_put_string wrote to leveldb database");
    
    char *value = levelzdb_get_string (lz, "key", 3);
    test (streq (value, "value"), "levelzdb_get_item retrieved correct value");
    free (value);

    levelzdb_del_key (lz, "key", 3);
    test ((lz->result == 0), "levelzdb_del_item deleted from leveldb database");

    zframe_t *frame_key = zframe_new ("zkey", 4);
    zframe_t *frame_val = zframe_new ("zvalue", 6);
    levelzdb_put_zframe (lz, frame_key, frame_val);
    test ((lz->result == 0), "levelzdb_put_frame wrote to leveldb database");

    zframe_t *frame_check = levelzdb_get_zframe (lz, frame_key);
    char *check = zframe_strdup (frame_check);
    test (streq (check, "zvalue"), "levelzdb_get_zframe retrieved correct value");

    free (value);
    free (check);

    zframe_destroy (&frame_key);
    zframe_destroy (&frame_val);
    zframe_destroy (&frame_check);
    
    levelzdb_close_db (lz);
    levelzdb_destroy_db (lz);
    levelzdb_free (&lz);  
    return EXIT_SUCCESS;

failed:
    return EXIT_FAILURE;
}
